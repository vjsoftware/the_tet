import React, { Component } from 'react';
import Popular from './components/Popular';
import { Row, Col } from 'reactstrap';
import './home.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {

    }
  }

  componentWillMount() {
    // console.log(this.props.match.url);
  }
  
  render() {
    return (
      <div>
        <article >
          <header>
            <Row>
              <Col xs="5" className="ml-5 mt-4">
                <h3>the Tet</h3>
              </Col>
              <Col xs="6" className="mt-4">
                <ul className="list-inline">
                  <li className="list-inline-item px-3"><a href="/">Welcome, venky vJ</a></li>
                  <li className="list-inline-item px-3"><a href="/">Read Later <span className="read_later_btn ml-1">5</span></a></li>
                </ul>
              </Col>
            </Row>
          </header>
          <nav>
            <ul className="list-inline ml-5">
              <li className="list-inline-item h5 font-weight-normal"><a href="/">Mobiles</a></li>
              <li className="list-inline-item h5 font-weight-normal px-4"><a href="/">Watches</a></li>
            </ul>
          </nav>
        </article>

        <Popular />

        {/* <div className="row">
          <div className="col-md-2">hi </div>      <div className="col-md-2">hello </div>
        </div> */}
      </div>

    );
  }
}

export default App;
