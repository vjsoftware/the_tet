import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Switch, BrowserRouter } from 'react-router-dom';
import { ModalContainer } from 'react-router-modal';
import Home from './Home';
import Dashboard from './components/admin/Dashboard';
import AddNews from './components/admin/AddNews';

// import 'react-router-modal/css/react-router-modal.css';


class Routes extends Component {
    render() {

        return (
            <BrowserRouter>
                <div className="App">

                    <div className="App-main">

                        <div className="App-content">
                            <Switch>
                                <Route path="/admin/add" component={AddNews} />
                                <Route path="/admin" exact component={Dashboard} />
                                <Route path="/" component={Home} />
                            </Switch>
                        </div>
                    </div>

                    <ModalContainer
                        bodyModalOpenClassName=''
                        containerClassName=''
                        backdropClassName=''
                        modalClassName=''
                    />
                </div>

            </BrowserRouter>

        )
    }
}

export default Routes;