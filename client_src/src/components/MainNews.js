import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ModalRoute } from "react-router-modal";
import { Col, Modal, ModalHeader, ModalBody } from "reactstrap";
import Spinner from "./Spinner";
import Img from "react-image";
import axios from "axios";
// import scrollToElement from 'scroll-to-element';
// import Scroll from 'react-scroll';
// import { scroller } from 'react-scroll';

class MainNews extends Component {
  constructor(props) {
    super(props);

    this.state = { data: props.data, modal: false, newsPopularSkip: [] };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
    window.history.pushState("page2", "Title", "/");
  }

  componentWillMount() {
    // console.log(this.props);
    this.getNewsPopularSkip();
  }

  getNewsPopularSkip() {
    axios
      .get(
        "http://localhost:3000/api/news?filter[skip]=2&filter[limit]=2&filter[order]=id DESC"
      )
      .then(response => {
        this.setState({
          newsPopularSkip: response.data
        });
      });
  }


  render() {
    const newsItemsPopularSkip = this.state.newsPopularSkip.map((news, i) => {
      const desc = news.description;
      return <article key={news.id} className="col-md-6" onClick={e => this.setLoading(news.id)} style={{ paddingLeft: 3, paddingRight: 3 }}>
          <Img src={"http://localhost:3000/api/storages/news/download/" + news.image} className="img-fluid" />
          <h6 className="mt-2">{desc.substring(0, 90)}</h6>
          <div className="time">
            <i className="icon-time" />
            {news.date}
          </div>
          {/* <div className="time_border" /> */}
        </article>;
    });

    const newsModal = this.props.data.map((mainnews, i) => {
      const tit = mainnews.title;
      const desc = mainnews.description;
      return (
        <div key={mainnews.id}>
          <Modal
            isOpen={this.state.modal}
            toggle={this.toggle}
            className="modalStyle"
          >
            <ModalHeader toggle={this.toggle}>
              <span className="">{tit}.</span>
            </ModalHeader>
            <ModalBody>
              <article id="sct" className="element">
                <Img
                  src={
                    "http://localhost:3000/api/storages/news/download/" +
                    mainnews.image
                  }
                  className="img-fluid px-3"
                />

                <div className="mx-3 px-3">
                  <p className="mt-2">{desc}</p>
                  <time>posted: {this.props.url}</time>
                </div>
              </article>
            </ModalBody>
          </Modal>
        </div>
      );
    });
    function MainModal() {
      return <div className="basic__modal-content">{newsModal}</div>;
    }
    const news = this.props.data.map((mainnews, i) => {
      const desc = mainnews.description;
      const tit = mainnews.title;
      return <div>
          <article id="" className="" key={mainnews.id}>
            <div className="">
              <div className="tdorder" />
              <h5 className="mb-2 font-weight-bold black">Trending</h5>
            </div>
            <Img src={"http://localhost:3000/api/storages/news/download/" + mainnews.image} className="img-fluid" />

            <div className="article_content mx-3 px-3">
              <Link to={`${tit}`} onClick={this.toggle}>
                <h5 className="mt-2">{tit.substring(0, 60)}.</h5>
                <h6 className="">{desc.substring(0, 170)}</h6>
                <h6>posted: {mainnews.date}</h6>
              </Link>
            </div>
            <ModalRoute component={MainModal} exact path={`*/${tit}`} />
          </article>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <div className="row">{newsItemsPopularSkip}</div>
        </div>;
    });

    var content;
    if (this.props.loading === "true") {
      content = <Spinner />;
    } else {
      content = news;
    }

    return <Col className="px-3">{content}
    
      </Col>;
  }
}

export default MainNews;
