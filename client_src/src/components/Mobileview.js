import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import axios from 'axios';
import Img from 'react-image';

class Mobileview extends Component {
    constructor(props, ...rest) {
        super(props, ...rest);

        this.renderThumb = this.renderThumb.bind(this);

        this.state = {
            top: 0,
            data: [],
            news: [],
            newsId: ''
        };
    }

    componentWillMount() {
        this.getMainNews();
        this.getNews();
    }

    componentDidMount() {

    }

    renderThumb({ style, ...props }) {
        const thumbStyle = {
            backgroundColor: '#f1c40f',
        };
        return (
            <div
                style={{ ...style, ...thumbStyle }}
                {...props} />
        );
    }

    getMainNews() {
        axios.get("http://localhost:3000/api/news?filter[limit]=1&filter[order]=id DESC").then(response => {
            this.setState({
                data: response.data
            });
        });
    }

    getNews() {
        axios.get("http://localhost:3000/api/news?filter[order]=id DESC").then(response => {
            this.setState({
                news: response.data
            });
        });
    }

    getNewsId(e) {
        axios.get('http://localhost:3000/api/news/' + e).then(response => {
            this.setState({
                data: [response.data]
            });
        });
    }
    render() {
        const newsItems = this.state.news.map((news, i) => {
            const desc = news.description;
            return (
                <article key={news.id} className="articles mb-4 cursor" onClick={e => this.getNewsId(news.id)}>
                    <Img src={"http://localhost:3000/api/storages/news/download/" + news.image} className="img-fluid" />
                    <h6 className="mt-2">{desc.substring(0, 90)}</h6>
                    <div className="time">
                        <i className="icon-time" />
                        {news.date}
                    </div>
                    <div className="time_border"></div>
                </article>
            )
        });
        return(
            <Row className="pt-3 popular">
                <div className="col-md-2">
                    <div className="col-md-12">
                        <div className="tdorder" />
                        <h5 className="mb-2 font-weight-bold black">Current</h5>
                    </div>
                    <div className="col-md-12">
                        <Col className="px-3">
                            {newsItems}

                        </Col>
                    </div>
                </div>

                <div className="col-md-2">
                    <div className="col-md-12">
                        <div className="tdorder" />
                        <h5 className="mb-2 font-weight-bold black">Popular</h5>
                    </div>
                    <div className="col-md-12">
                        <Col className="">
                            {newsItems}
                        </Col>
                    </div>
                </div>
            </Row>
        )
    }
}

export default Mobileview;