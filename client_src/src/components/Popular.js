import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import axios from "axios";
import MainNews from "./Mainnews";
import Mobileview from "./Mobileview";
import Img from "react-image";
import { Scrollbars } from "react-custom-scrollbars";
import PerfectScrollbar from 'perfect-scrollbar';

import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile
} from "react-device-detect";

// import scrollToElement from 'scroll-to-element';
// import scrollToComponent from 'react-scroll-to-component';

// const container = document.querySelector('#container');

const popularStyle = {
  height: window.innerHeight / 1.4,
};



class Popular extends Component {
  constructor(props, ...rest) {
    super(props, ...rest);
    // const ps = new PerfectScrollbar('#container');
    // ps.update();

    this.renderThumb = this.renderThumb.bind(this);

    this.state = {
      newNewsId: "",
      top: 0,
      isLoading: "",
      data: [],
      news: [],
      newsLatest: [],
      newsPopular: [],
      newsPopularSkip: [],
      newsId: ""
    };
  }

  componentWillMount() {
    this.getMainNews();
    this.getNewsLatest();
    this.getNewsPopular();
    this.getNewsPopularSkip();
  }

  componentDidMount() {

    // console.log(window.innerHeight / 1.4);
  }

  renderThumb({ style, ...props }) {
    const thumbStyle = {
      backgroundColor: "#f1c40f",
      height: "150px !important"
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  }

  getMainNews() {
    axios
      .get(
        "http://localhost:3000/api/news?filter[limit]=1&filter[order]=id DESC"
      )
      .then(response => {
        this.setState({
          data: response.data,
          newNewsId: response.data[0].id
        });
      }).catch(err => console.log(err));
  }

  getNewsLatest() {
    axios
      .get("http://localhost:3000/api/news?filter[limit]=3&filter[order]=id DESC")
      .then(response => {
        this.setState({
          newsLatest: response.data
        });
      });
  }
  getNewsPopular() {
    axios
      .get("http://localhost:3000/api/news?filter[limit]=2&filter[order]=id DESC")
      .then(response => {
        this.setState({
          newsPopular: response.data
        });
      });
  }
  getNewsPopularSkip() {
    axios
      .get("http://localhost:3000/api/news?filter[skip]=2&filter[limit]=2&filter[order]=id DESC")
      .then(response => {
        this.setState({
          newsPopularSkip: response.data
        });
      });
  }

  setLoading(e) {
    this.setState({ isLoading: "true", newNewsId: e });
    // console.log();
    setTimeout(() => {
      axios
        .get("http://localhost:3000/api/news/" + this.state.newNewsId)
        .then(response => {
          this.setState({ data: [response.data], isLoading: "false" });
        });
    }, 2000);
  }

  function2(e) {
    setTimeout(4000);
    console.log("iuiuiu");
    console.log(this.state.newNewsId);
  }

  getNewsId(e) {
    // console.log(e);
    // axios
    //   .get("http://localhost:3000/api/news/" + this.state.newNewsId)
    //   .then(response => {
    //     this.setState({
    //       data: [response.data]
    //     });
    //   });
    // console.log("opopop");
  }

  render() {
    

    const newsItemsLatest = this.state.newsLatest.map((news, i) => {
      const desc = news.description;
      return (
        
        
        <article
          key={news.id}
          className="articles mb-4 cursor"
          onClick={e => this.setLoading(news.id)}
        >
          <Img
            src={
              "http://localhost:3000/api/storages/news/download/" + news.image
            }
            className="img-fluid"
          />
          <h6 className="mt-2">{desc.substring(0, 90)}</h6>
          <div className="time">
            <i className="icon-time" />
            {news.date}
          </div>
          <div className="time_border" />
        </article>
      );
    });
    const newsItemsPopular = this.state.newsPopular.map((news, i) => {
      const desc = news.description;
      return (
        
        
        <article
          key={news.id}
          className="articles mb-4 cursor"
          onClick={e => this.setLoading(news.id)}
        >
          <Img
            src={
              "http://localhost:3000/api/storages/news/download/" + news.image
            }
            className="img-fluid"
          />
          <h6 className="mt-2">{desc.substring(0, 90)}</h6>
          <div className="time">
            <i className="icon-time" />
            {news.date}
          </div>
          <div className="time_border" />
        </article>
      );
    });
    const newsItemsPopularSkip = this.state.newsPopularSkip.map((news, i) => {
      const desc = news.description;
      return (
        
        
        <article
          key={news.id}
          className="articles mb-4 cursor"
          onClick={e => this.setLoading(news.id)}
        >
          <Img
            src={
              "http://localhost:3000/api/storages/news/download/" + news.image
            }
            className="img-fluid"
          />
          <h6 className="mt-2">{desc.substring(0, 90)}</h6>
          <div className="time">
            <i className="icon-time" />
            {news.date}
          </div>
          <div className="time_border" />
        </article>
      );
    });
    return <div id="container">
        <BrowserView device={isBrowser}>
          <Row className="pt-3 popular">
            <div className="col-md-3">
                <div className="tdorder" />
                <h5 className="mb-2 font-weight-bold black">Latest</h5>
              <Col className="px-3"> {newsItemsLatest} </Col>
            </div>
            <div className="col-md-5" style={{ paddingLeft: 0, paddingRight: 0 }}>
              <MainNews data={this.state.data} loading={this.state.isLoading} />
              
            </div>
            <div className="col-md-4">
              <div className="col-md-12">
                <div className="tdorder" />
                <h5 className="mb-2 font-weight-bold black">...</h5>
              </div>
              <Col className="">{newsItemsPopular}</Col>
            </div>
            {/* <div className="brder_left mx-1" /> */}
            {/* <div className="col-md-3 menulinks">
              <ul>
                <li>Tech 1 hour ago</li>
                <li>Mobiles 1 hour ago</li>
                <li>Games 1 hour ago</li>
                <li>Movies 1 hour ago</li>
                <li>Finance 1 hour ago</li>
                <li>Hacking's 1 hour ago</li>
                <li>Health 1 hour ago</li>
                <li>Microsoft 1 hour ago</li>
                <li>Apple 1 hour ago</li>
                <li>Politics 1 hour ago</li>
              </ul>
            </div> */}
          </Row>
        </BrowserView>
        <MobileView device={isMobile}>
          <Mobileview />
        </MobileView>
      </div>;
  }
}

export default Popular;
