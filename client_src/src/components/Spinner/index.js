import React from 'react';
// import SpinnerSvg from './puff.svg';
import './index.css';

const Spinner = props => (
  <div className="spinner">
    <img src={require('./puff.svg')} alt="Loading" />
  </div>
);

export default Spinner;