import React, { Component } from 'react';
import Header from './Header';
import { Button, Form, FormGroup, Label, Input, Row, Col } from 'reactstrap';
// import {Editor, EditorState} from 'draft-js';
import axios from 'axios'
// import RichTextEditor from 'react-rte';

class AddNews extends Component {
    constructor(props) {
        super(props);

        this.state = {
            category: '',
            title: '',
            slug: '',
            description: '',
            image: '',
            date: '',
            dataImage: []
        }
    }

    addImage(image) {
        axios
            .request({
                method: "post",
                url: "http://localhost:3000/api/storages/news/upload",
                data: image.file,
                headers: { 'Content-Type': 'multipart/form-data' }
            })
            .then(response => {
                console.log(response);
                console.log(response.data.result.files.image[0].name);
                const imagename = response.data.result.files.image[0].name;
                const news = {
                    category_id: this.state.category,
                    author: this.state.category,
                    title: this.state.title,
                    slug: this.state.slug,
                    description: this.state.description,
                    image: imagename,
                    date: this.state.date
                };
                this.addNews(news);
            })
            .catch(err => console.log(err.response.data));
    }

    addNews(news) {
        axios
            .request({
                method: "post",
                url: "http://localhost:3000/api/news",
                data: news,
            })
            .then(response => {
                this.props.history.push("/admin/news/add");
            })
            .catch(err => console.log(err.response.data));
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onSubmit = e => {
        e.preventDefault();
        var formData = new FormData();
        var imagefile = document.querySelector('#image');
        formData.append("image", imagefile.files[0]);
        formData.append("imageText", 'nnnn');
        const image = {
            file: formData
        };
        this.addImage(image);
    }

    render() {
        return (
            <div>
                <Header />
                <br />
                <Form encType="multipart/form-data">
                    <div xs="12">
                        <Row>
                            <Col xs="3">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={4}>Category</Label>
                                    <Col xs={12}>
                                        <Input type="text" name="category" id="category" onChange={e => this.handleChange(e)} placeholder="Category" />
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col xs="3">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={4}>Title</Label>
                                    <Col xs={12}>
                                        <Input type="text" name="title" id="title" onChange={e => this.handleChange(e)} placeholder="Article Title" />
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col xs="3">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={4}>Slug</Label>
                                    <Col xs={12}>
                                        <Input type="text" name="slug" id="slug" onChange={e => this.handleChange(e)} placeholder="Article Slug" />
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col md="1">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={1}>.</Label>
                                    <Col xs={12}>
                                        <Button color="primary" type="submit" onClick={(e) => this.onSubmit(e)}>Add</Button>
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="9">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={1}>Description</Label>
                                    <Col xs={12}>
                                        <Input type="textarea" name="description" id="description" onChange={e => this.handleChange(e)} placeholder="Article Description" />
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="3">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={4}>File</Label>
                                    <Col xs={12}>
                                        <Input type="file" name="image" id="image" />
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col xs="3">
                                <FormGroup>
                                    <Label for="exampleEmail" xs={4}>Date</Label>
                                    <Col xs={12}>
                                        <Input type="date" name="date" id="date" onChange={e => this.handleChange(e)} placeholder="Date" />
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>
                    </div>
                </Form>
            </div>
        );
    }
}

export default AddNews;