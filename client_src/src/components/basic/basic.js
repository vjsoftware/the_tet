import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';
import './index.css';

function World() {
  return (
    <div className='basic__modal-content'>
      <h4>*/world</h4>
      <p>
        This is shown for any path that ends with /world
      </p>
    </div>
  );
}

class BasicExample extends Component {
  constructor(props) {
    super(props);

    this.state = {

    }
  }
  render() {
    const match = this.props.match.url
    return (
      <div>
        <h5>Basic Example</h5>
        <p>
          <a href='https://github.com/davidmfoley/react-router-modal-examples/blob/master/src/examples/basic/'>View Source</a>
        </p>
        <p>
          In this example, two ModalRoutes are defined, one that matches /hello and another that matches */world.
        </p>
        <p>
          Depending on the route, either or both of the modals is shown.
        </p>
        <p>
          <Link to={`${match}/crazy/world`}>./crazy/world</Link>
        </p>
  
        <ModalRoute component={World} exact path={`*/world`} />
      </div>
    );
  }
}

export default BasicExample;
