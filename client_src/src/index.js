import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import Routes from './Routes';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css';
import 'react-router-modal/css/react-router-modal.css';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Routes />, document.getElementById('root'));
registerServiceWorker();
